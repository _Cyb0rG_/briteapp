﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using briteapp.CustClass;

namespace briteapp
{
    /// <summary>
    /// Interaction logic for LoginWin.xaml
    /// </summary>
    public partial class LoginWin : Window
    {
        public LoginWin()
        {
            InitializeComponent();
        }

        private void FirstBordNextBtn_Click(object sender, RoutedEventArgs e)
        {
            //Need to check if fields empty or filled
            pDet.Foreground = (Brush)FindResource("DisabledColor");
            pDetBor.Fill = (Brush)FindResource("DisabledColor");
            oDet.Foreground = (Brush)FindResource("MainColor");
            oDetBor.Fill = (Brush)FindResource("MainColor");
            FirstBorder.Visibility = Visibility.Collapsed;
            SecondBorder.Visibility = Visibility.Visible;
            ComboBoxItem semBox = (ComboBoxItem)SemDrop.SelectedItem;
            string sem = "cse" + semBox.Content.ToString();
            sQL.LoadTable($"select Course_code, Course_name from {sem}", ref dt);
            MonSubList.ItemsSource = dt.DefaultView;
            MonSubList.DisplayMemberPath = "Course_name";
        }

        DataTable dt = new DataTable();
        SQLSetup sQL = new SQLSetup();

        private void MonNextBtn_Click(object sender, RoutedEventArgs e)
        {
            MondaySection.Visibility = Visibility.Collapsed;
            TuesdaySection.Visibility = Visibility.Visible;
            QuickViewPnl.Children.RemoveRange(0, 20);
        }

        private void TueNextBtn_Click(object sender, RoutedEventArgs e)
        {
            TuesdaySection.Visibility = Visibility.Collapsed;
            QuickViewPnl.Children.RemoveRange(0, 20);
            WedSection.Visibility = Visibility.Visible;
        }

        private void TeuPrevBtn_Click(object sender, RoutedEventArgs e)
        {
            TuesdaySection.Visibility = Visibility.Collapsed;
            QuickViewPnl.Children.RemoveRange(0, 20);
            MondaySection.Visibility = Visibility.Visible;
        }

        private void WedNextBtn_Click(object sender, RoutedEventArgs e)
        {
            WedSection.Visibility = Visibility.Collapsed;
            QuickViewPnl.Children.RemoveRange(0, 20);
            ThursSection.Visibility = Visibility.Visible;
        }

        private void WedPrevBtn_Click(object sender, RoutedEventArgs e)
        {
            WedSection.Visibility = Visibility.Collapsed;
            QuickViewPnl.Children.RemoveRange(0, 20);
            TuesdaySection.Visibility = Visibility.Visible;
        }

        private void ThuNextBtn_Click(object sender, RoutedEventArgs e)
        {
            QuickViewPnl.Children.RemoveRange(0, 20);
            ThursSection.Visibility = Visibility.Collapsed;
            FriSection.Visibility = Visibility.Visible;
        }

        private void ThuPrevBtn_Click(object sender, RoutedEventArgs e)
        {
            ThursSection.Visibility = Visibility.Collapsed;
            QuickViewPnl.Children.RemoveRange(0, 20);
            WedSection.Visibility = Visibility.Visible;
        }

        private void FriPrevBtn_Click(object sender, RoutedEventArgs e)
        {
            FriSection.Visibility = Visibility.Collapsed;
            QuickViewPnl.Children.RemoveRange(0, 20);
            ThursSection.Visibility = Visibility.Visible;
        }

        private MaterialDesignThemes.Wpf.Chip ChipMaker(string cont)
        {
            var myChip = new MaterialDesignThemes.Wpf.Chip()
            {
                Height = 50,
                Content = cont,
                IsDeletable = true,
                Margin = new Thickness(10)
            };
            return myChip;
        }

        private void MonSubList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView selIt = (DataRowView)MonSubList.SelectedItem;
            string sem = selIt.Row[0].ToString();
            QuickViewPnl.Children.Add(ChipMaker(sem));
        }

        private void TueSubList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView selIt = (DataRowView)MonSubList.SelectedItem;
            string sem = selIt.Row[0].ToString();
            QuickViewPnl.Children.Add(ChipMaker(sem));
        }

        private void WedSubList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView selIt = (DataRowView)MonSubList.SelectedItem;
            string sem = selIt.Row[0].ToString();
            QuickViewPnl.Children.Add(ChipMaker(sem));
        }

        private void ThuSubList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView selIt = (DataRowView)MonSubList.SelectedItem;
            string sem = selIt.Row[0].ToString();
            QuickViewPnl.Children.Add(ChipMaker(sem));
        }

        private void FriSubList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView selIt = (DataRowView)MonSubList.SelectedItem;
            string sem = selIt.Row[0].ToString();
            QuickViewPnl.Children.Add(ChipMaker(sem));
        }
    }
}
